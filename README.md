# Spring Boot + Swagger 개발

## 목차
1. [설정 및 확인](#1.-설정-및-확인)
2. [swagger 사용](#2.-swagger-사용)
3. [결과](#3.-결과)

## 1.설정 및 확인
1. 의존성 추가
```java
// https://mvnrepository.com/artifact/io.springfox/springfox-boot-starter
implementation group: 'io.springfox', name: 'springfox-boot-starter', version: '3.0.0'
```

2. controller api 개발 및 확인

<details>
<summary>결과 이미지</summary>
<div markdown="1">

![](img/swagger_check.jpg)
</div>
</details>

## 2.swagger 사용
1. api 명 
```java
@Api(tags = {"API 정보를 제공하는 Controller :)"})
```

2. 메소드 명
```java
@ApiOperation(value = "사용자의 이름과 나이를 리턴하는 메소드")
```

3. 상세 설명

`controller`
```java
@ApiResponse(code= 502, message = "사용자의 나이가 10살 이하일때")
```

`dto`
```java
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    @ApiModelProperty(value = "사용자의 이름", example = "hyunho", required = true)
    private String name;
    @ApiModelProperty(value = "사용자의 나이", example = "10", required = true)
    private int age;
}
```

4. 기본 값 설정
```java
@ApiImplicitParams(
        {
            @ApiImplicitParam(name ="x", value = "x값", required = true, dataType = "int", paramType = "path"),
            @ApiImplicitParam(name ="y", value = "y값", required = true, dataType = "int", paramType = "query")
        }
```

## 3.결과
![](img/swagger_detail.jpg)
